import csv

def csvWriter(csvFile,content):
    with open(csvFile, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(content)

def csvReader(csvFile):
    with open(csvFile) as csv_file:
        csv_reader = csv.reader(csv_file)
        line_count = 0
        current_list = []
        for row in csv_reader:
            current_list.append(row[0])
            line_count += 1
    return current_list

def csvRemover(csvFile, content):
    lines = list()
    with open(csvFile, 'r') as inp:
        for row in csv.reader(inp):
            lines.append(row)
            print(lines)
            for field in row:
                if field == content:
                    lines.remove(row)
    with open(csvFile, 'w') as out:
        writer = csv.writer(out)
        writer.writerows(lines)
