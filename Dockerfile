FROM python:3.8
LABEL maintainer ahmza

RUN apt update && apt install cron git -y
RUN git clone https://github.com/Rapptz/discord.py
WORKDIR /discord.py
RUN python3 -m pip install -U .[voice]
WORKDIR /
COPY requirements.txt .
RUN pip install -r requirements.txt
WORKDIR /app
COPY torrent-crontab /etc/cron.d/crontab
RUN chmod 0644 /etc/cron.d/crontab
COPY . .

RUN /usr/bin/crontab /etc/cron.d/crontab

ENTRYPOINT ["python3"]
CMD ["bot.py"]
