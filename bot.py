#!/bin/python

import csv
import random
import discord

from discord.ext import commands
from lib.csvTools import csvReader, csvWriter, csvRemover
from lib.getToken import getToken as token
from lib.torrentgrip import torrentlist


## Setup intents for the server
intents = discord.Intents.default()
intents.members = True


## Initialize the bot command and assign prefix aswel as intents 
bot = commands.Bot(command_prefix='g.', intents=intents)


########################################################
#### Boring Bot Init
########################################################

## checks if server is up
@bot.event
async def on_ready():
    print('Script successfully logged in')
    await bot.change_presence(activity=discord.Game(name="With your feelings"))


## Checks to see if a member has joined the guild
@bot.event
async def on_member_join(member):
    print(f'{member} has joined the server')


## Checks to see if a member has left the guild
@bot.event
async def on_member_remove(member):
    print(f'{member} has left the server')


########################################################
#### Fun Stuff
########################################################

@bot.command(aliases=['🏓'],brief='Pings my heart to check if im alive')
async def ping(ctx):
    await ctx.send(f":ping_pong: Pong! {round(bot.latency * 1000)}ms")

@bot.command(aliases=['🎱', '8ball'], brief='The 🎱 decides your fate')
async def _8ball(ctx, *, question=""):
    if question == "":
        await ctx.send("You need to ask 8ball a question.")
    else:
        await ctx.send(f'Question: {question}\nAnswer:   {random.choice(csvReader("./data/lists/eightBall-responses.csv"))}')

########################################################
#### Torrent helper
########################################################

@bot.command(aliases=['🏴‍☠️'], brief='Torrent alerting/searching', description='Torrent alerting/searching, uses RSS feeds so still a WIP to get more Feeds implemented. Great for upcoming releases')
async def find(ctx, *, title=""):
    title = title.lower()
    title = title.replace(" ", ".")
    titlels = []
    titlels.append(title.lower())
    if title == "":
        await ctx.send("Listen kid, you need to tell me what to find")
    else:
        curList=csvReader("./data/lists/torrentlist.csv")
        if any(title in word for word in curList):
            await ctx.send('Entry already exists, Checking torrent availability')
        else:
            csvWriter("./data/lists/torrentlist.csv", titlels)
            await ctx.send(f'{title} has been added to the list')
        await ctx.send(f'```Heres the results:\n{chr(10).join(torrentlist(title))}```')
        await ctx.send('if no results found then Keep an eye out for the automatic checks')

@bot.command(brief='Allows you to delete torrent alert entries')
async def findrm(ctx, *, title=""):
    title = title.lower()
    title = title.replace(" ", ".")
    if title == "":
        await ctx.send("This command requires input. If you are unsure about the names try g.findls")
    else:
        csvRemover("./data/lists/torrentlist.csv", title)
        await ctx.send(f'{title} has been removed from the watch list')

@bot.command(brief='Displays torrents you want me to look for')
async def findls(ctx):
    await ctx.send(f'```{chr(10).join(csvReader("./data/lists/torrentlist.csv"))}```')

########################################################
#### Moderator Stuff
########################################################

## Clear the chat
@bot.command(brief='Allows mods to clear the chat')
@commands.has_permissions(manage_messages=True)
async def clear(ctx, amount=5):
    if amount == 0:
        await ctx.send("dont be silly")
    else:
        await ctx.channel.purge(limit=amount+1)


## Kick Users
@bot.command(brief='Allows mods to kick users')
@commands.has_role('moderator')
async def kick(ctx, member : discord.Member, *, reason=None):
    await member.kick(reason=reason)
    await ctx.send(f"Banned {member.mention}. Reason: {reason}")


## Ban Users
@bot.command(brief='Allows mods to ban users')
@commands.has_role('moderator')
async def ban(ctx, member : discord.Member, *, reason=None):
    await member.ban(reason=reason)
    await ctx.send(f"Banned {member.mention}. Reason: {reason}")


## Unban Users
@bot.command(brief='Allows mods to unban users')
@commands.has_role('moderator')
async def unban(ctx, *, member):
    bannedUsers = await ctx.guild.bans()
    memberName, memberdiscriminator = member.split('#')

    for banEntry in bannedUsers:
        user = banEntry.user

        if (user.name, user.discriminator) == (memberName, memberdiscriminator):
            await ctx.guild.unban(user)
            await ctx.send(f"Unbanned {user.mention}")
            return

@bot.command(brief='Change your server nickname with this tool.')
async def nick(ctx, member : discord.Member, *, nickname):
    await member.edit(nick=nickname)
    await ctx.send(f"Nickname changed for {member.mention}")


########################################################
#### Cogs
########################################################

# Need to work on cogs

bot.run(token())



